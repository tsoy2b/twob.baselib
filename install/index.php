<?

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

Loc::loadMessages(__FILE__);

class twob_baselib extends \CModule {
	public function __construct() {
		$arModuleVersion = array();
		include __DIR__ . '/version.php';
		if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
			$this->MODULE_VERSION = $arModuleVersion['VERSION'];
			$this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
		}

		$this->MODULE_ID = 'twob.baselib';
		$this->MODULE_NAME = Loc::getMessage('THE_MODULE_NAME');
		$this->MODULE_DESCRIPTION = Loc::getMessage('MODULE_DESCRIPTION');
		$this->MODULE_GROUP_RIGHTS = 'N';
		$this->PARTNER_NAME = Loc::getMessage('MODULE_PARTNER_NAME');
		$this->PARTNER_URI = 'http://2bstudio.kz';
	}

	public function doInstall() {
		ModuleManager::registerModule($this->MODULE_ID);
		if ( ! Loader::includeModule($this->MODULE_ID))
			return;

	}

	public function doUninstall(){
		if ( ! Loader::includeModule($this->MODULE_ID))
			return;
		ModuleManager::unregisterModule($this->MODULE_ID);
	}
}
