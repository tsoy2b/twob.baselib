<?
namespace TwoB;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

class Basket {

		// Returns sum of all basket items quantities
	public static function getAllItemsQuantity(){
		$currentFuser = \Bitrix\Sale\Fuser::getId(true);
		if (!$currentFuser ) return 0;

		$fullBasket = \Bitrix\Sale\Basket::loadItemsForFUser($currentFuser, SITE_ID);//, $this->getSiteId());
		if ($fullBasket->isEmpty())
			return 0;

		$basketItemList = [];

		$basketClone = $fullBasket->createClone();
		$orderableBasket = $basketClone->getOrderableItems();

		if ($orderableBasket->isEmpty()) return;

		$qty = 0;
		foreach ($orderableBasket as $basketItem){
			$qty += $basketItem->getField('QUANTITY');
		}
		return $qty;
	}
}
