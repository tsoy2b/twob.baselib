<?
namespace TwoB;

class Currency {
	public static function getList(){
		$currencies = [];
		$currencyIterator = \Bitrix\Currency\CurrencyTable::getList([ 'select' => ['CURRENCY']]);
		while ($currency = $currencyIterator->fetch()) {
			$currencyFormat = \CCurrencyLang::GetCurrencyFormat($currency['CURRENCY'], LANG_ID);
			$currencies[] = [
				'CURRENCY' => $currency['CURRENCY'],
				'FORMAT' => [
					'FORMAT_STRING' => $currencyFormat['FORMAT_STRING'],
					'DEC_POINT' => $currencyFormat['DEC_POINT'],
					'THOUSANDS_SEP' => $currencyFormat['THOUSANDS_SEP'],
					'DECIMALS' => $currencyFormat['DECIMALS'],
					'THOUSANDS_VARIANT' => $currencyFormat['THOUSANDS_VARIANT'],
					'HIDE_ZERO' => $currencyFormat['HIDE_ZERO']
				]
			];
		}
		return $currencies;
	}
}
