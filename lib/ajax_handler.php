<?
namespace TwoB;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

class AjaxHandler {
	const BitrixSessId 		= false;
	const CallInterval 		= 0;
	const EventModuleName	= false;
	const EventName			= false;

	public function __construct(){
		define('STOP_STATISTICS', true);
		define('NO_KEEP_STATISTIC', 'Y');
		define('NO_AGENT_STATISTIC','Y');
		define('DisableEventsCheck', true);
		define('BX_SECURITY_SHOW_MESSAGE', true);


		$this->request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
		$this->request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);
		$this->preProcess();

		if ( static::BitrixSessId )
			$this->checkSessId();

		if ( static::CallInterval )
			$this->checkCallInterval();

		if ( static::EventModuleName && static::EventName )
			$this->execEvent(static::EventModuleName, static::EventName, $this->getParams());
		$this->successResponse();
	}

		// Do some before the the process if required
	public function preProcess(){
	}

		// Check bitrix session id if provided
	public function checkSessId(){
		if ( ! check_bitrix_sessid(static::BitrixSessId) )
			Baselib::responseJson(array('error'=>array('Обновите страницу')));
	}

		// Check calling interval limit if provided
	public function checkCallInterval(){
		$sentTime = $_SESSION['SMAN_AJAX_HANDLER'.get_called_class()];
		if ( static::CallInterval && $sentTime && ((new \DateTime())->getTimestamp() - $sentTime) < intval(static::CallInterval) )
			Baselib::responseJson(array('error'=>array('Интервал не прошел')));

		$_SESSION['SMAN_AJAX_HANDLER'.get_called_class()] = (new \DateTime())->getTimestamp();
	}

		// Execute event
	public function execEvent($moduleName, $eventName, $params){
		if ( ! Baselib::execEvent($moduleName, $eventName, $params) )
			Baselib::responseJson(array('error' => 'Internal error'));
	}

		// Collect paramters to be sent to the event
	public function getParams(){
		return [];
	}


	public function successResponse(){
		\Twob\Baselib::responseJson(array('success'=>'DONE'));
	}
}
