<?
namespace TwoB;

class NoDataInCacheException extends \Exception{};

class BaseLib{

		// Execute event
	public static function execEvent($moduleName, $eventName, $params){
		foreach(\Bitrix\Main\EventManager::getInstance()->findEventHandlers($moduleName, $eventName) as $arHandler){
			if(!ExecuteModuleEvent($arHandler, $params)) return false;
		}
		return true;
	}

	public static function getCache($timeout, $id, $func = NULL){
		$cache = \Bitrix\Main\Data\Cache::createInstance(); 
		if ($cache->initCache($timeout, $id) )
			return $cache->getVars();

		if ( ! is_callable($func) )
			throw new NoDataInCacheException();

		$data = $func();

		if ( $cache->startDataCache() )
		    $cache->endDataCache($data);

		return $data;
	}



	public static function blog($text, $module_id, $traceDepth, $showArgs){
		AddMessage2Log($text, $module_id, $traceDepth, $showArgs);
	}

	public static function blogAr($text, $ar){
		AddMessage2Log($text .': '.json_encode($ar));
	}


	//plural(1001,array('товар', 'товара', 'товаров'));
	public static function plural($number, Array $titles){
		$num = abs((int)str_replace("-","",$number));
		return $titles[$num % 10 == 1 && $num % 100 != 11 ? 0 : ( $num % 10 >= 2 && $num % 10 <= 4 && ($num % 100 < 10 || $num % 100 >= 20) ? 1 : 2)];
	}

	public static function GetDefaultCurrency(){
		if(!CModule::IncludeModule("sale")) return false;

		$obCache = new CPHPCache;
		$life_time = 36000;
		$cache_params = array('option'=>'getOption');
		$cache_params['module'] = 'sale';
		$cache_params['option'] = 'default_currency';

		$cache_id = md5(serialize($cache_params));
		if($obCache->InitCache($life_time, $cache_id, "/")){
			$arResult = $obCache->GetVars();
		} else {
			$arResult['CURRENCY'] = COption::GetOptionString("sale", "default_currency","RUB");
		}

		if($obCache->StartDataCache()){
			$obCache->EndDataCache($arResult);
		}
		return $arResult['CURRENCY'];
	}



	public static function responseJson($data){
		header('Content-Type:application/json; charset=UTF-8');
		//echo \Bitrix\Main\Web\Json::encode($result);
		echo(json_encode($data));
		\CMain::FinalActions();
		die();
	}

	public static function getDirContentMTime($dir){
		return array_reduce(
			glob($dir),
			function($sum, $path){ return $sum + filemtime($path);},
			0
		);
	}

	public static function compileCss(string $srcPath, string $dstPath, string $srcDir){
		$meta = self::getDirContentMTime($srcDir);
		$metaFile = $dstPath.'.meta';

		if ( $meta != file_get_contents($metaFile) ){
			exec(sprintf('/usr/local/bin/sassc -m %s %s 2>&1', $srcPath, $dstPath), $output, $ret);
			if ( $ret ) {
				$GLOBALS['APPLICATION']->RestartBuffer();
				echo '<pre>'.implode(PHP_EOL, $output);
				exit;
			}
			file_put_contents($metaFile, $meta);
		}
	}

	public static function isMobile(){
		require_once(__DIR__.'/MobileDetect/Mobile_Detect.php');
		return (new \Mobile_Detect())->isMobile();
	}
}
