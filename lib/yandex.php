<?
namespace TwoB;

class Yandex{
	public static function translate($key, $lang, $text){

		$url = sprintf("https://translate.yandex.net/api/v1.5/tr.json/translate?key=%s&text=%s&lang=%s", $key, urlencode($text), $lang);

		$curlObject = curl_init();
		curl_setopt($curlObject, CURLOPT_URL, $url);
		curl_setopt($curlObject, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curlObject, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curlObject, CURLOPT_RETURNTRANSFER, true);
		$responseData = curl_exec($curlObject);
		curl_close($curlObject);
		if ($responseData === false)
			throw new Exception('YandexTranslate: Null response');

		return json_decode($responseData);
	}
}
