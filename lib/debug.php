<?
namespace TwoB;

class Debug{
	public static function dumpAllUserFields(){
		$ar=\CUserTypeEntity::getList(); 
		while ($i=$ar->GetNext() )
			print_r($i); 
	}

	public static function backTraceFiles(){
		dd('Tracing files', false);
		foreach(debug_backtrace() as $trac){
			var_dump($trac['file'].' : '.$trac['line']);
		}
		exit;
	}
}
