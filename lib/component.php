<?
namespace TwoB;

use Bitrix\Main\Context;

class Component extends \CBitrixComponent{
	var $haveAjax = false;

	public function __construct($component = null){
		parent::__construct($component);

		$this->APP = $GLOBALS['APPLICATION'];
//		$this->context	= Context::getCurrent();
//		$this->request	= $this->context->getRequest();

//		$this->request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
//		$this->response = Bitrix\Main\Application::getInstance()->getContext()->getResponse();
//		\Bitrix\Main\Application::getInstance()->getContext()->getRequest()->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);

		$this->checkAjax();
	}

	protected function restartBuffer(){
		$this->APP->RestartBuffer();
	}

	protected function checkAjax(){
		if ( ! $this->haveAjax )
			return;

		$this->arResult['AJAX_ID'] = \CAjax::GetComponentID($this->__name, $this->__template->__name, '');

		if ( $this->request->isAjaxRequest() ){
			$this->restartBuffer();
			if ( $this->request->getPost('AJAX_ID') != $this->arResult['AJAX_ID'] ) die('z');
		}
	}

	protected function getUserById($id){
		return CUser::GetByID(intval($id))->Fetch();
	}

	protected function getMessage($text, $params){
		return strlen(GetMessage($text, $params))
			? GetMessage($text, $params)
			: $text;
	}

	protected function internalErrorJson(){
		Baselib::responseJson(array('errors' => array($this->getMessage('Internal error'))));
	}

		// Check calling interval
	public function checkCallInterval($interval){
		$sentTime = $_SESSION['CALL_INTERVAL_'.get_called_class()];
		if ( $interval && $sentTime && ((new \DateTime())->getTimestamp() - $sentTime) < intval($interval) )
			return false;

		$_SESSION['CALL_INTERVAL_'.get_called_class()] = (new \DateTime())->getTimestamp();
		return true;
	}
}
