<?

\Bitrix\Main\Loader::registerAutoLoadClasses('twob.baselib', [
	'\TwoB\Baselib'		=> 'lib/baselib.php',
	'\TwoB\AjaxHandler'	=> 'lib/ajax_handler.php',
	'\TwoB\Component'	=> 'lib/component.php',
	'\TwoB\Yandex'		=> 'lib/yandex.php',
	'\TwoB\Debug'		=> 'lib/debug.php',
	'\TwoB\Currency'	=> 'lib/currency.php',
	'\TwoB\Basket'		=> 'lib/basket.php',
]);

function dd($data, $exit = true){
	ini_set('xdebug.var_display_max_data ', '10240');
	ini_set('xdebug.var_display_max_depth', '10');
	ini_set('xdebug.var_display_max_children', '10240');
	$GLOBALS['APPLICATION']->RestartBuffer();
	$trace = debug_backtrace(false,1);
	var_dump(sprintf('%s:%s', $trace[0]['file'], $trace[0]['line']));
	var_dump($data); 
	if ( $exit ) exit;
}

function pp($data, $exit = true){ 
	$GLOBALS['APPLICATION']->RestartBuffer();
	$trace = debug_backtrace(false,1);
	print_r(sprintf('%s:%s', $trace[0]['file'], $trace[0]['line']));
	print_r(sprintf('%s:%s', $trace[0]['file'], $trace[0]['line']).PHP_EOL);
	print_r($data); 
	if ( $exit ) exit;
}

if ( ! function_exists('__') ){
	function __($msg){
//		IncludeTemplateLangFile(debug_backtrace()[0]['file']);
		return _($msg);
		$tMsg = GetMessage($msg);
		return $tMsg ? $tMsg : $msg;
	}
}

function getSectionFieldValue($ar, $field){
	return $ar[LANG_ID=='ru' ? $field : sprintf('UF_%s_%s', $field, LANG_UC_ID)];
}

function getElementFieldValue($ar, $field){
	if ( LANG_ID=='ru' ) return $ar[$field];

	return $ar['PROPERTIES'][sprintf('%s_%s', $field, LANG_UC_ID)]['VALUE'];
}
